package com.tt.tools.logsimulation;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

public class SearchLogSimulationThread extends LogSimulationThread {

	private static final Logger LOGGER = Logger.getLogger(SearchLogSimulationThread.class);
	
	private Format formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");

	public SearchLogSimulationThread(int threadId, LogFileMapping lfm, Date startTime) throws Exception {
			super(threadId, lfm, startTime);
			
		}

	public String constructNewLoggingRow(long newTime, ParsedLogRow parsedLogRow) {
		return ("\"" + formatter.format(newTime) + "\"" + parsedLogRow.getAfter());
	}

	// "2018/08/17 13:12:58.672";"10.60.195.23";"sxi
	public ParsedLogRow getParsedLogRow(String row) {
		try {
			String before = "\"";
			String dateStr = row.substring(1, row.indexOf(';') - 1);
			String after = row.substring(row.indexOf(';'));
			
			Date rowTime = (Date) formatter.parseObject(dateStr);
			
			ParsedLogRow parsedLogRow = new ParsedLogRow(before, rowTime, after);
	
			return parsedLogRow;
		} catch (Exception x) {
			LOGGER.debug(x.getMessage());
			return null;
		}
	}

}

