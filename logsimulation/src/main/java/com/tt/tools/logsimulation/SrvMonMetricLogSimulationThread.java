package com.tt.tools.logsimulation;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

public class SrvMonMetricLogSimulationThread extends LogSimulationThread {

	private static final Logger LOGGER = Logger.getLogger(SrvMonMetricLogSimulationThread.class);
	
	private Format formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
	private Format altFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");

	public SrvMonMetricLogSimulationThread(int threadId, LogFileMapping lfm, Date startTime) throws Exception {
			super(threadId, lfm, startTime);
			
		}

	public String constructNewLoggingRow(long newTime, ParsedLogRow parsedLogRow) {
		return (formatter.format(newTime) + parsedLogRow.getAfter());
	}

	// 2018-05-28T02:56:39.911+02:00;srv14290.hm.
	// 2018-07-15T00:01:32.205+0200
	
	//2018-05-17T02:50:18.944+02:00;0;SUMMARY
	//2018-05-17T02:50:24+02:00;0;SUMMARY
	public ParsedLogRow getParsedLogRow(String row) {
		try {
			String before = "";
			String dateStr = row.substring(0, row.indexOf(';'));
			String after = row.substring(row.indexOf(';'));
			
			Date rowTime;
			try {
				rowTime = (Date) formatter.parseObject(dateStr);
			} catch (Exception e) {
				rowTime = (Date) altFormatter.parseObject(dateStr);
			}
			
			ParsedLogRow parsedLogRow = new ParsedLogRow(before, rowTime, after);
	
			return parsedLogRow;
		} catch (Exception x) {
			LOGGER.debug(x.getMessage() + "   Row=" + row);
			return null;
		}
	}

}
