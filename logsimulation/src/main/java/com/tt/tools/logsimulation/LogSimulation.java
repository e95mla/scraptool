package com.tt.tools.logsimulation;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

public class LogSimulation {

	private static final Logger LOGGER = Logger.getLogger(LogSimulation.class);

	private static String PROPERTY_FILE = "log-simulation.properties";
	private static String MAPPING_FILE = "log-file-mappings.properties";

	private static boolean doLogging = false;

	private static long timeDeltaToAddToLogTimestamps = 0;
	private static boolean keepRealisticLoggingPace = true;
	
	private static final String PROPERTY_SOURCE_START_TIME = "log.source.start.timestamp";
	private static final String PROPERTY_TARGET_START_TIME = "log.target.start.timestamp";
	private static final String PROPERTY_KEEP_REALISTIC_LOGGING_PACE = "log.keep.realistic.logging.pace";
	
	private String sourceStartTimestamp = "2000-01-01T12:12:12.121+0200";
	private String targetStartTimestamp = "";
	
	
	private List<LogFileMapping> logFiles = new ArrayList<LogFileMapping>();

	private List<LogSimulationThread> threads = new ArrayList<LogSimulationThread>();
	
	public LogSimulation() throws Exception {

		initProperties();
		readUpFileMappingsConfig();
		
		if(targetStartTimestamp != null && targetStartTimestamp.trim().length() > 0) {
        	keepRealisticLoggingPace = false;
        }
		
		LOGGER.info("*** sourceStartTimestamp                : " + sourceStartTimestamp);
		LOGGER.info("*** targetStartTimestamp                : " + targetStartTimestamp);
		
		LOGGER.info("*** keepRealisticLoggingPace            : " + keepRealisticLoggingPace);
		
		Format formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		Date sourceStartTime = (Date) formatter.parseObject(sourceStartTimestamp);

		int counter = 0;

		// Start up all threads and have them find their file starting point based on the given start timestamp.
		for (LogFileMapping lfm : this.logFiles) {

			if (lfm.getLogFileType().equals("access")) {
				LogSimulationThread thread = new AccessLogSimulationThread(++counter, lfm, sourceStartTime);
				thread.start();
				threads.add(thread);
				LOGGER.debug("ACCESS log thread started");
			} else if (lfm.getLogFileType().equals("json")) {
				LogSimulationThread thread = new JsonLogSimulationThread(++counter, lfm, sourceStartTime);
				thread.start();
				threads.add(thread);
				LOGGER.debug("JSON_LOG log thread started");
			} else if (lfm.getLogFileType().equals("srvmon_metric")) {
				LogSimulationThread thread = new SrvMonMetricLogSimulationThread(++counter, lfm, sourceStartTime);
				thread.start();
				threads.add(thread);
				LOGGER.debug("SRVMON_METRIC log thread started");
			} else if (lfm.getLogFileType().equals("search")) {
				LogSimulationThread thread = new SearchLogSimulationThread(++counter, lfm, sourceStartTime);
				thread.start();
				threads.add(thread);
				LOGGER.debug("SEARCH log thread started");
			}
		}
		
		while(true) {
			// Check that everyone has found the time position, or that they have not given the files available.
			LOGGER.info("Checking and waiting for all threads to reach the starting time position.");
			boolean areAllThreadsReadyToBegin = false;
			while(!areAllThreadsReadyToBegin) {
				areAllThreadsReadyToBegin = true;
				Thread.sleep(3000);
				for(LogSimulationThread thread : this.threads) {
					if(thread.isAlive() && !thread.hasFoundStartTimestamp()) {
						LOGGER.debug("At least Thread [" + thread.getThreadId() +"] is NOT yet ready to begin.");
						areAllThreadsReadyToBegin = false;
						break;
					}
				}
			}
			LOGGER.info("Ready to begin.");
			
			// Find out time delta that all loggers should add to the current timings
			if(this.targetStartTimestamp != null && !this.targetStartTimestamp.trim().equals("")) {
				Date targetStartTime = (Date) formatter.parseObject(targetStartTimestamp);
				timeDeltaToAddToLogTimestamps = targetStartTime.getTime() - sourceStartTime.getTime();
			} else {
				timeDeltaToAddToLogTimestamps = System.currentTimeMillis() - sourceStartTime.getTime();
			}
			
			long days = TimeUnit.MILLISECONDS.toDays(timeDeltaToAddToLogTimestamps);
			long hours = TimeUnit.MILLISECONDS.toHours(timeDeltaToAddToLogTimestamps - TimeUnit.DAYS.toMillis(days));
			long minutes = TimeUnit.MILLISECONDS.toMinutes(timeDeltaToAddToLogTimestamps - TimeUnit.DAYS.toMillis(days) - TimeUnit.HOURS.toMillis(hours));
			long seconds = TimeUnit.MILLISECONDS.toSeconds(timeDeltaToAddToLogTimestamps - TimeUnit.DAYS.toMillis(days) - TimeUnit.HOURS.toMillis(hours) - TimeUnit.MINUTES.toMillis(minutes));
			 
			LOGGER.info("*** timeDeltaToAddToLogTimestamps     : " + timeDeltaToAddToLogTimestamps);
			LOGGER.info("***       days                        : " + days);
			LOGGER.info("***       hours                       : " + hours);
			LOGGER.info("***       minutes                     : " + minutes);
			LOGGER.info("***       seconds                     : " + seconds);
			
			// Tell all threads it is OK to start logging.
			doLogging = true;
			
			LOGGER.info("Start logging.");
			
			// Check if any thread as run out of rows. To keep the monitoring synched all threads
			// need to stop and start from the beginning again.
			while(doLogging) {
				Thread.sleep(500);
			}
		}		
	}

	public static void main(String[] args) throws Exception {
		new LogSimulation();
	}

	private void readUpFileMappingsConfig() throws IOException {
		LOGGER.debug("Read up log file mappings....");

		try (BufferedReader br = new BufferedReader(new FileReader(MAPPING_FILE))) {
			String row;
			while ((row = br.readLine()) != null) {
				StringTokenizer tokens = new StringTokenizer(row, "|");
				if (tokens.countTokens() == 3) {
					String logFileType = tokens.nextToken().trim();
					LogFileMapping lfm;
					if(logFileType.equals("srvmon_metric")) {
						lfm = new LogFileMapping(logFileType, tokens.nextToken().trim(), tokens.nextToken().trim(), false);
					}else {
						lfm = new LogFileMapping(logFileType, tokens.nextToken().trim(), tokens.nextToken().trim(), true);
					}
					LOGGER.debug(lfm);
					this.logFiles.add(lfm);
					LOGGER.info("Number of files: [" + lfm.getFromDirectory() + "/" + lfm.getFromFilenamePattern()
							+ "] = " + lfm.getFileList().size());
				}
			}
		}
		LOGGER.debug("Size of log mapping list = " + this.logFiles.size());
	}
	
	private void initProperties() throws IOException {
		LOGGER.info("Initiate properties...");
        Properties props = loadProperties(PROPERTY_FILE);

        sourceStartTimestamp = props.getProperty(PROPERTY_SOURCE_START_TIME, String.valueOf(sourceStartTimestamp));
        targetStartTimestamp = props.getProperty(PROPERTY_TARGET_START_TIME, String.valueOf(targetStartTimestamp));
        
        keepRealisticLoggingPace = Boolean.parseBoolean(props.getProperty(PROPERTY_KEEP_REALISTIC_LOGGING_PACE, String.valueOf(keepRealisticLoggingPace)));
	}
	
	private Properties loadProperties(String propsFile) throws IOException {
		LOGGER.debug("Loading property file: " + propsFile);
        InputStream in = this.getClass().getClassLoader().getResourceAsStream(propsFile);
        LOGGER.debug("    Stream: " + in);
        Properties props = new Properties();
        try {
            props.load(in);
        } catch (Exception x) {
        	LOGGER.error(x, x);
        } finally {
            in.close();
        }
        return props;
    }
	
	public static long getTimeDeltaToAddToLogTimestamps() {
		return timeDeltaToAddToLogTimestamps;
	}
	
	public static boolean shouldKeepRealisticLoggingPace() {
		return keepRealisticLoggingPace;
	}

	public static boolean doLogging() {
		return doLogging;
	}
	
	public static void doLogging(boolean doLogging) {
		LOGGER.info("Setting doLogging to : " + doLogging);
		LogSimulation.doLogging = doLogging;
	}
	

}
