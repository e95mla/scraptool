package com.tt.tools.logsimulation;

import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.log4j.Logger;

public class LogFileMapping {
	
	private static final Logger LOGGER = Logger.getLogger(LogSimulation.class);
	
	
	private String id;
	private Path fromDirectory;
	private String fromFilenamePattern;
	private List<Path> fileList = new ArrayList<Path>();
	private String toPath;
	private String logFileType;
	private boolean compareAscending = true;

	public LogFileMapping(String logFileType, String fromPattern, String toPath, boolean compareAscending) {
		this.toPath = toPath;
		this.logFileType = logFileType;
		this.compareAscending = compareAscending;
		
		this.fromDirectory = Paths.get(fromPattern.substring(0, fromPattern.lastIndexOf('/')));
		this.fromFilenamePattern = fromPattern.substring(fromPattern.lastIndexOf('/')+1);
		
		LOGGER.trace("this.fromDirectory="+this.fromDirectory);
		LOGGER.trace("this.fromFilenamePattern="+this.fromFilenamePattern);
		LOGGER.trace("this.toPath="+this.toPath);
		
		FileSystem fs = FileSystems.getDefault();
		final PathMatcher matcher = fs.getPathMatcher("glob:" + fromFilenamePattern);
		final List<Path> files=new ArrayList<>();
		try {
			Files.walkFileTree(fromDirectory, new SimpleFileVisitor<Path>(){
			    @Override
			    public FileVisitResult visitFile(Path file, BasicFileAttributes attribs) {
			        Path name = file.getFileName();
			        if (matcher.matches(name)) {
			        	LOGGER.trace(String.format("Found matched file: '%s'.%n", file));
			        	files.add(file);
			        }
			        return FileVisitResult.CONTINUE;
			    }
			});
		} catch (IOException e) {
			LOGGER.error("Failed finding given file and path: [" + fromPattern + "]");
		}
		
		this.fileList = files;
		Collections.sort(this.fileList, new Comparator<Path>() {
            @Override
            public int compare(Path p1, Path p2) {
                String s1 = p1.getFileName().toString();
                String s2 = p2.getFileName().toString();
                return s1.compareToIgnoreCase(s2);
            }
        });
		
		if(!this.compareAscending) {
			Collections.reverse(this.fileList);
		}
		
		LOGGER.trace("Total number of files=" + this.fileList.size());
	}
	
	public void isCompareAscending(boolean compareAscending) {
		this.compareAscending = compareAscending;
	}
	public String getLogFileType() {
		return logFileType;
	}
	public void setLogFileType(String logFileType) {
		this.logFileType = logFileType;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public List<Path> getFileList() {
		return fileList;
	}
	public void setFileList(List<Path> fileList) {
		this.fileList = fileList;
	}
	public String getToPath() {
		return toPath;
	}
	public void setToPath(String toPath) {
		this.toPath = toPath;
	}
	public Path getFromDirectory() {
		return fromDirectory;
	}
	public void setFromDirectory(Path fromDirectory) {
		this.fromDirectory = fromDirectory;
	}
	public String getFromFilenamePattern() {
		return fromFilenamePattern;
	}
	public void setFromFilenamePattern(String fromFilenamePattern) {
		this.fromFilenamePattern = fromFilenamePattern;
	}
	public String toString() {
		return ("fromPath=" + this.fromDirectory + " : toPath=" + this.toPath);
	}
	

}
