package com.tt.tools.logsimulation;

import java.util.Date;

import org.apache.log4j.Logger;

public class JsonLogSimulationThread extends LogSimulationThread {

	private static final Logger LOGGER = Logger.getLogger(JsonLogSimulationThread.class);

	public JsonLogSimulationThread(int threadId, LogFileMapping lfm, Date startTime) throws Exception {
			super(threadId, lfm, startTime);
			
		}

	public String constructNewLoggingRow(long newTime, ParsedLogRow parsedLogRow) {
		return (parsedLogRow.getBefore() + ":" + newTime + parsedLogRow.getAfter());
	}

	// {"timeMillis":1526767289092,"thread":"pool-2-thread-1","level":"DEBUG
	public ParsedLogRow getParsedLogRow(String row) {
		try {
			String before = row.substring(0, row.indexOf(':'));
			String dateStr = row.substring(row.indexOf(':') + 1, row.indexOf(','));
			String after = row.substring(row.indexOf(','));
			
			Date rowTime = new Date(Long.parseLong(dateStr));
			
			ParsedLogRow parsedLogRow = new ParsedLogRow(before, rowTime, after);
	
			return parsedLogRow;
		} catch (Exception x) {
			LOGGER.debug(x.getMessage());
			return null;
		}
	}

}
