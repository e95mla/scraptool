package com.tt.tools.logsimulation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Date;

import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.RollingFileAppender;

public abstract class LogSimulationThread extends Thread {

	private static final Logger LOGGER = Logger.getLogger(LogSimulationThread.class);
	
	private Logger TARGET_LOGGER = null;

	LogFileMapping lfm;
	int threadId;
	Date startTime;
	boolean foundStartTimestamp = false;

	public LogSimulationThread(int threadId, LogFileMapping lfm, Date startTime) throws Exception {
		this.lfm = lfm;
		this.threadId = threadId;
		this.startTime = startTime;
	}

	public void run() {

		try {
			initLogger();
			
			while (true) {
				boolean searchForStartingPoint = true;
				for (Path path : lfm.getFileList()) {
					LOGGER.trace("THREAD=" + this.threadId + " : " + path.toString());
					
					FileInputStream input = new FileInputStream(new File(path.toString()));
			        CharsetDecoder decoder = Charset.forName("UTF-8").newDecoder();
			        decoder.onMalformedInput(CodingErrorAction.REPLACE);
			        InputStreamReader reader_ = new InputStreamReader(input, decoder);
			        BufferedReader reader = new BufferedReader(reader_);
	
					//BufferedReader reader = Files.newBufferedReader(path, Charset.forName("UTF-8"));
					String row = null;
					while ((row = reader.readLine()) != null) {
						ParsedLogRow parsedLogRow = getParsedLogRow(row);
						if (parsedLogRow != null) {
			
							// Looking for starting point in the source log files
							if (searchForStartingPoint && parsedLogRow.getRowTimestamp().after(startTime)) {
	
								LOGGER.info("FOUND START TIME: startTime=" + startTime + ", rowTime=" + parsedLogRow.getRowTimestamp());
								LOGGER.info("     thread=" + this.threadId + ", name= " + this.lfm.getFromFilenamePattern());
								LOGGER.info("     row=" + row);
	
								LOGGER.info("     Waiting for other threads to get ready...");
								searchForStartingPoint = false;
								foundStartTimestamp = true;
								while (!LogSimulation.doLogging()) {
									Thread.sleep(100);
								}
							}
							if (!searchForStartingPoint) {
								//make check so the logs are written 'about' the same time as in the new timestamp
								long newTime = parsedLogRow.getRowTimestamp().getTime() + LogSimulation.getTimeDeltaToAddToLogTimestamps();
								
								// Log the entry at correct pace
								if(LogSimulation.shouldKeepRealisticLoggingPace()) {
									while(newTime > System.currentTimeMillis()) {
										Thread.sleep(50);
									}
								}
								
								TARGET_LOGGER.info(constructNewLoggingRow(newTime, parsedLogRow));
							}
						}
						if(!searchForStartingPoint && !LogSimulation.doLogging()) {
							LOGGER.info("Rewind signal received for [thread=" + this.threadId + ", pattern= " + this.lfm.getFromFilenamePattern() + "]");
							break;
						}
					}
					reader.close();
				}
				if(foundStartTimestamp) {
					LOGGER.warn("All log files have been parsed. Ending thread. [thread=" + this.threadId + ", pattern= " + this.lfm.getFromFilenamePattern() + "]");
					
					if(!LogSimulation.shouldKeepRealisticLoggingPace()) {
						LOGGER.warn("EXITING Thread [thread=" + this.threadId + ", pattern= " + this.lfm.getFromFilenamePattern() + "]");
						break;
					}
					LOGGER.warn("    Sending signal for all threads to rewind and restart");
					foundStartTimestamp = false;
					LogSimulation.doLogging(false);
				} else {
					LOGGER.error("Start timestamp NOT found in ANY of the files. Exiting thread");
					break;
				}
			}
			
		} catch (Exception e) {
			LOGGER.error(e, e);
		}
	}
	
	public abstract String constructNewLoggingRow(long newTime, ParsedLogRow parsedLogRow);
	public abstract ParsedLogRow getParsedLogRow(String row);
	
	private void initLogger() {
		TARGET_LOGGER = Logger.getLogger(this.lfm.getToPath());
		
		RollingFileAppender fa = new RollingFileAppender();
		fa.setFile(lfm.getToPath());
		fa.setLayout(new PatternLayout("%m%n"));
		fa.setAppend(true);
		fa.activateOptions();
		fa.setMaxBackupIndex(2);
		fa.setMaxFileSize("10MB");

		TARGET_LOGGER.addAppender(fa);		
	}

	public boolean hasFoundStartTimestamp() {
		return this.foundStartTimestamp;
	}

	public int getThreadId() {
		return threadId;
	}

}
