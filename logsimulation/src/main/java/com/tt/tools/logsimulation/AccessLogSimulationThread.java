package com.tt.tools.logsimulation;

import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

public class AccessLogSimulationThread extends LogSimulationThread {
	
	private static final Logger LOGGER = Logger.getLogger(AccessLogSimulationThread.class);
	
	private Format formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

	public AccessLogSimulationThread(int threadId, LogFileMapping lfm, Date startTime) throws Exception {
		super(threadId, lfm, startTime);
	}
	
	public String constructNewLoggingRow(long newTime, ParsedLogRow parsedLogRow) {
		return (parsedLogRow.getBefore() + "|" + formatter.format(newTime) + "|" + parsedLogRow.getAfter());
	}
	
	// 2018-07-15T00:01:32.205+0200
	public ParsedLogRow getParsedLogRow(String row) {
		StringTokenizer tokens = new StringTokenizer(row, "|");
		if (tokens.countTokens() > 2) {
			String before = tokens.nextToken();
			String dateStr = tokens.nextToken().trim();

			Date rowTime;
			try {
				rowTime = (Date) formatter.parseObject(dateStr);
			} catch (ParseException e) {
				LOGGER.error(e.getMessage());
				return null;
			}
			
			String[] as = row.split("\\|", -1);
			String after = row.substring(row.indexOf(as[2]));
			
			ParsedLogRow parsedLogRow = new ParsedLogRow(before, rowTime, after);
			
			return parsedLogRow;
		} else {
			return null;
		}
	}

}
