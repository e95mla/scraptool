package com.tt.tools.logsimulation;

import java.util.Date;

public class ParsedLogRow {
	
	private String before;
	private String after;
	private Date rowTimestamp;
	
	public ParsedLogRow(String before, Date rowTimestamp, String after) {
		this.before = before;
		this.after = after;
		this.rowTimestamp = rowTimestamp;
	}

	public String getBefore() {
		return before;
	}

	public void setBefore(String before) {
		this.before = before;
	}

	public String getAfter() {
		return after;
	}

	public void setAfter(String after) {
		this.after = after;
	}

	public Date getRowTimestamp() {
		return rowTimestamp;
	}

	public void setRowTimestamp(Date rowTimestamp) {
		this.rowTimestamp = rowTimestamp;
	}

}
