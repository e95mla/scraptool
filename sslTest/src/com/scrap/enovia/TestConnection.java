package com.scrap.enovia;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import matrix.db.Context;
import matrix.db.MQLCommand;

public class TestConnection {
	public static void main(String[] args) {
		Context context = null;
		try {
			
			createSSLTrustingHostnameVerifier();
			createSSLCertificateChainIgnorer();

			if (args.length == 0) {
				context = new Context("https://3dx.vs.com/3dspace");
				context.setUser("creator");
				context.setPassword("");

			} else {
				context = new Context(args[0]);
				context.setUser(args[1]);
				if (args.length > 2) {
					context.setPassword(args[2]);
				} else {
					context.setPassword("");
				}
			}

			System.out.println("context.connect() ...");

			context.connect();

			System.out.println("OK");

			MQLCommand mql = new MQLCommand();

			System.out.println("list vault ...");

			mql.executeCommand(context, "monitor context");
			System.out.println(mql.getResult());
			System.out.println("DONE");
		} catch (Exception x) {
			x.printStackTrace();
		}
	}

	private static void createSSLTrustingHostnameVerifier() {
		HostnameVerifier allHostsValid = new HostnameVerifier() {

			@Override
			public boolean verify(String hostname, SSLSession session) {
				return true;
			}
		};

		// Install the all-trusting host verifier
		HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
	}

	private static void createSSLCertificateChainIgnorer() throws NoSuchAlgorithmException, KeyManagementException {
		// Create a trust manager that does not validate certificate chains
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {

			@Override
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			@Override
			public void checkClientTrusted(X509Certificate[] certs, String authType) {
			}

			@Override
			public void checkServerTrusted(X509Certificate[] certs, String authType) {
			}
		} };

		// Install the all-trusting trust manager
		SSLContext sc = SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, new java.security.SecureRandom());
		HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
	}
}
